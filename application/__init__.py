from flask import Flask
from .controllers import load_controllers
def create_app():
    app = Flask(__name__)
    register_blueprints(app)
    return app

def register_blueprints(app):
    for item in load_controllers():
        app.register_blueprint(item.controller)