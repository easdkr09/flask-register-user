from flask import (
    Blueprint, current_app, request, render_template
)

controller = Blueprint('hello', __name__, url_prefix='/hello')

@controller.route('')
def main_req():
    return "hello"