from flask import (
    Blueprint, current_app, request, render_template
)

controller = Blueprint('main', __name__, url_prefix='/main')

@controller.route('')
def main_req():
    return "main"